function distance(first, second){
	if (first instanceof Array && second instanceof Array){
		var result = [];
	  	for (var i = 0; i < first.length; i++) {
	    	if (second.indexOf(first[i]) === -1) {
	      		result.push(first[i]);
	    	}
	  	}
	  	for (i = 0; i < second.length; i++) {
	    	if (first.indexOf(second[i]) === -1) {
	      		result.push(second[i]);
	    	}
	  	}
	  	const unique = new Set(result);
	  	return unique.size;
	  }
	  else{
	  	throw new Error("InvalidType");
	  }
}

module.exports.distance = distance